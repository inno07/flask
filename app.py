import json
import os

import csv
from os.path import isfile, join

import requests
from flask import Flask, flash, request, redirect, url_for, send_file, Response
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = 'uploads/'
ALLOWED_EXTENSIONS = {'txt', 'csv'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/api')
def hello_world():  # put application's code here
    return 'Hello World!'


@app.route('/api/tensorflow/<id>/<filename>', methods=['GET'])
def createModel(id, filename):  # put application's code here
    x = request.args.get("x")
    y = request.args.get("y")
    filename = filename + '.csv'
    with open('uploads/'+id+'/'+filename, 'rb') as f:
        # For DockerCompose
        #'http://tensorflow:5050/tensorflow?x={}&y={}'
        # 127.0.0.1:5000/api/tensorflow/32152/ai4i2020?x=Torque [Nm]&y=Rotational speed [rpm]
        response = requests.post('http://tensorflow:5050/tensorflow?x={}&y={}'.format(x, y), files={'file': f})

        return _corsify_actual_response(Response(response.content, status=response.status_code, mimetype=response.headers.get("Content-Type")))


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/api/uploads/<id>/<filename>', methods=['GET'])
def getCSV(id, filename):  # put application's code here
    path = os.path.join(app.config['UPLOAD_FOLDER'], id, filename)
    isExist = os.path.exists(path)
    if isExist:
        try:
            return _corsify_actual_response(send_file(path, attachment_filename=filename))
        # Datei löschen
        except Exception as e:
            return str(e)
    return _corsify_actual_response(Response("error", status=404, mimetype='application/json'))


@app.route('/api/fetch/<id>', methods=['GET'])
def getCSVs(id):  # put application's code here
    path = os.path.join(app.config['UPLOAD_FOLDER'], id)
    isExist = os.path.exists(path)
    list = []
    if isExist:
        headers = []
        onlyfiles = [f for f in os.listdir(path) if isfile(join(path, f))]
        for i in onlyfiles:
            with open(os.path.join(path, i), newline='') as f:
                reader = csv.reader(f)
                i = i.rsplit(".", 1)
                header = next(reader)
                print(header[0])
                list += [{
                    "title": i[0],
                    "headers": header
                }]
                print(i[0])
        return _corsify_actual_response(Response(json.dumps(list), status=200, mimetype='application/json'))
    return _corsify_actual_response(Response(json.dumps(list), status=204, mimetype='application/json'))


# curl -F 'file=@ai4i2020.csv' -F 'userID=23' localhost:5000/api/upload
@app.route('/api/upload', methods=['POST'])
def upload():
    if 'file' in request.files:
        file = request.files['file']

        #muss geändert werden
        if 'userID' in request.form:
            id = request.form['userID']
        else:
            return _corsify_actual_response(Response("No UserID", status=400, mimetype='application/json'))
    else:
        print('No file part')
        return _corsify_actual_response(Response("No File", status=400, mimetype='application/json'))
    if file.filename == '':
        print('No selected file')
        return _corsify_actual_response(Response("No Filename", status=400, mimetype='application/json'))
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        userid = secure_filename(id)
        path = os.path.join(app.config['UPLOAD_FOLDER'], userid)
        isExist = os.path.exists(path)

        if not isExist:
            os.makedirs(path)
            print("The new directory is created!")

        file.save(os.path.join(path, filename))

        with open(os.path.join(path, filename), newline='') as f:
            reader = csv.reader(f)
            filename = filename.rsplit(".", 1)
            header = next(reader)
            print(header[0])
            list = {
                "title": filename[0],
                "headers": header
            }
            print(list)
        return _corsify_actual_response(Response(json.dumps(list), status=201, mimetype='application/json'))
    return _corsify_actual_response(Response("Wrong FileType", status=400, mimetype='application/json'))

@app.route('/api/classification', methods=['POST'])
def classification():  # put application's code here
    trainDataFile = request.files['trainFile']
    predictDataFile = request.files['dataFile']
    dependentCols = request.args.get("dependentCols")
    predict_colname = request.args.get("parameterCol")
    limit = request.args.get("limit")

    if trainDataFile and predictDataFile and dependentCols and predict_colname:
        if not limit:
            response = requests.post('http://tensorflow:5050/classification?dependentCols={}&parameterCol={}'.format(dependentCols, predict_colname), files={'trainFile': trainDataFile, 'dataFile': predictDataFile})
            return _corsify_actual_response(Response(response.content, status=response.status_code, mimetype=response.headers.get("Content-Type")))
        else:
            if limit:
                response = requests.post('http://tensorflow:5050/classificationNoArg?dependentCols={}&parameterCol={}&limit={}'.format(dependentCols,predict_colname, limit),files={'trainFile': trainDataFile, 'dataFile': predictDataFile})
                return _corsify_actual_response(Response(response.content, status=response.status_code, mimetype=response.headers.get("Content-Type")))

    return _corsify_actual_response(Response("error", status=404, mimetype='application/json'))


def _corsify_actual_response(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


if __name__ == '__main__':
    app.run(port=5000, debug=True, host='0.0.0.0')
